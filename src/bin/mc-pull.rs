use std::env;
use std::path::{Path, PathBuf};
use clap::Parser;
use mcpull::controller;

#[derive(Parser, Debug)]
#[command(author, version, about, long_about = None)]
struct Args {
    /// Name of the person to greet
    #[arg(short, long)]
    path: Option<String>,
}

fn get_current_working_dir() -> std::io::Result<PathBuf> {
    env::current_dir()
}

fn main() {
    let args = Args::parse();
    let mut json_path = get_current_working_dir().unwrap();
    json_path.push(PathBuf::from("package.json"));
    if !args.path.is_none() {
        json_path = PathBuf::from(args.path.unwrap());
        if json_path.is_dir() {
            json_path.push("package.json")
        }
    }
    controller::execute(&json_path).unwrap();
}
