use std::path::PathBuf;

use crate::dto::config::Config;
use crate::parser::parse_from_file;
use crate::resolvers::get_resolvers;
use crate::resolvers::Resolver;
use crate::transformer;
use crate::validator::{validate_json, validate_path};
use crate::work_queue::WorkQueue;

pub fn execute(path: &PathBuf) -> Result<(), String> {
    if !validate_path(path) {
        return Err(format!("No json found at: {}", path.to_str().unwrap_or("Unknown path!")));
    }
    let json_config = parse_from_file(path)
        .and_then(|value| validate_json(value))?;
    let mut config = transformer::into_config(&json_config);
    resolve_subdependencies(&mut config);
    Ok(())
}

// needs actual error handling :)
pub fn resolve_subdependencies(config: &mut Config) {
    let mut work_queue = WorkQueue::new();
    work_queue.push_all(&mut config.dependencies);
    let mut resolvers: Vec<Box<dyn Resolver>> = get_resolvers(config.resolvers.as_slice());
    while !work_queue.is_empty() {
        let package = work_queue.pop().unwrap();
        let resolver = resolvers.iter_mut().filter(|mut resolver| resolver.is_present(&package)).next();
        let mut dependency_list = resolver.map(|resolve| resolve.get_dependencies(&package)).unwrap_or(Vec::new());
        work_queue.push_all(&mut dependency_list);
        config.dependencies.push(package);
    }
}