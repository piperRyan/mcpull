use super::package::Package;

pub struct Config {
    pub resolvers: Vec<String>,
    pub dependencies: Vec<Package>
}

impl Config {
    pub fn new(resolvers: Vec<String>, dependencies: Vec<Package>) -> Config {
        Config {
            resolvers,
            dependencies
        }
    }
}