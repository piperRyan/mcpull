#[derive(Clone)]
pub struct Package {
    pub name: String,
    pub version: String,
    pub id: Option<String>
}

impl Package {
    pub fn new(name: String, version: String, id: Option<String>) -> Package {
        Package {
            name,
            version,
            id
        }
    }
}