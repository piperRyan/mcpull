use std::fs;
use std::path::{PathBuf};
use serde_json::{Value};

pub fn parse_from_file(path: &PathBuf) -> Result<Value, String> {
    println!("Reading file from {}", path.to_str().unwrap());
    fs::read_to_string(path)
        .map_err(|err| format!("Unable to read the file due to the following reason: \n {}", err))
        .and_then(|str| {
            match serde_json::from_str::<Value>(&*str) {
                Ok(val) => Ok(val),
                Err(err) => Err(format!("Unable to parse json due to the following reason: \n {}", err))
            }
        })
}