use crate::dto::package::Package;
use crate::resolvers::modrinth::Modrinth;

pub mod modrinth;


pub trait Resolver {
    fn is_present(&mut self, package: &Package) -> bool;
    fn get_dependencies(&mut self, start: &Package) -> Vec<Package>;

}

pub fn get_resolvers(resolver_names: &[String]) -> Vec<Box<dyn Resolver>> {
    let mut resolvers: Vec<Box<dyn Resolver>> = Vec::new();
    for name in resolver_names {
        let deref_name = name.as_str();
        match deref_name {
             _ => resolvers.push(Box::new(Modrinth::new())),
        }
    }
    resolvers
}

