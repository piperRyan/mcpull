use std::collections::{BTreeMap};
use crate::dto::package::Package;
use crate::resolvers::Resolver;
use reqwest::blocking::Client;
use serde_json::{Map, Value};

pub struct Modrinth {
    client: Client,
    base_url: String,
    in_mem_cache: BTreeMap<String, Value>
}

impl Modrinth {
    pub fn new() -> Modrinth {
        Modrinth{
            client: Client::new(),
            base_url: "https://api.modrinth.com/v2/".to_owned(),
            in_mem_cache: BTreeMap::new()
        }
    }
}

impl Resolver for Modrinth {
    fn is_present(&mut self, package: &Package) -> bool {
        let url = format!("{}/search?query={}", self.base_url, package.name);
        let mut response_json = Value::Object(Map::new());
        self.client.get(url).json(&response_json).send().unwrap();
        let base_obj = response_json.as_object()
            .and_then(|val| val.get("hits"))
            .and_then(|val| val.as_array())
            .and_then(|arr| arr.get(0))
            .and_then(|val| val.as_object());
        let package_name = base_obj.and_then(|map| map.get("slug"))
            .and_then(|str| str.as_str())
            .unwrap_or("");
        let is_version_present = base_obj.and_then(|map| map.get("versions"))
            .and_then(|ver| ver.as_array())
            .and_then(|ver| Some(ver.iter().filter(|ver_value| ver_value.as_str().unwrap_or("") == package.version)))
            .is_some();
        package_name == package.name && is_version_present
    }

    fn get_dependencies(&mut self, package: &Package) -> Vec<Package> {
        let url = format!("{}/search?query={}", &*self.base_url, package.name);
        let response = self.in_mem_cache.entry(url.clone()).or_insert_with(|| {
            let mut non_cached_map = Value::Object(Map::new());
            self.client.get(&url).json(&non_cached_map).send().unwrap();
            non_cached_map
        });
        let default = Vec::new();
        let dependency_map = Value::Object(Map::new());
        let dependencies = response.as_array()
            .and_then(|arr| arr.get(0))
            .and_then(|val| val.as_object())
            .and_then(|map| map.get("project_id"))
            .and_then(|id| id.as_str())
            .and_then(|id| Some(format!("{}/project/{}/dependencies", self.base_url, id)))
            .and_then(|url| {
                self.client.get(&url).json(&dependency_map).send().unwrap();
                Some(&dependency_map)
            })
            .and_then(|map| map.get("projects"))
            .and_then(|projects| projects.as_array())
            .unwrap_or(&default);
        dependencies.iter()
            .filter(|val| va)
            .map(|val| val.as_object().unwrap())
            .map(|val| {
                let slug = val.get("slug").and_then(|str| str.as_str()).unwrap();
                let version = val.get("versions").and_then(|versions| versions.as_array()).and_then(|arr| arr.first()).and_then(|val| val.as_str()).unwrap();
                let id = val.get("id").and_then(|id| id.as_str()).map(|str| str.to_owned());
                Package::new(slug.to_owned(), version.to_owned(), id)
            }).collect()
    }
}

