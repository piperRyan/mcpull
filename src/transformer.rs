use crate::dto::config::Config;
use crate::dto::package::Package;
use serde_json::Value;

pub fn into_config(json_val: &Value) -> Config {
    let json_map = json_val.as_object().unwrap();
    let resolvers: Vec<String> =  get_resolvers(json_map.get("resolvers").unwrap());
    let dependencies: Vec<Package> = get_dependencies(json_map.get("dependencies").unwrap());
    Config::new(resolvers, dependencies)
}

fn get_resolvers(json_val: &Value) -> Vec<String> {
        json_val
        .as_array().unwrap()
        .iter()
        .map(|val| val.as_str().unwrap())
        .map(|str| str.to_owned())
        .collect()
}

fn get_dependencies(json_val: &Value) -> Vec<Package> {
    let json_map = json_val.as_object().unwrap();
    json_map.iter().map(|pair| Package::new(pair.0.clone(), pair.1.as_str().unwrap().to_owned(), Option::None)).collect()
}