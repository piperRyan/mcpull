use std::path::PathBuf;
use serde_json::Value;
pub fn validate_path(path: &PathBuf) -> bool {
    path.exists() && path.extension().and_then(|os_str| os_str.to_str()).unwrap_or("") == "json"
}

pub fn validate_json(config_val: Value) -> Result<Value, String> {
    if let Value::Object(map) = &config_val {
        if !map.contains_key("resolvers") {
            Err("Missing required field \"resolvers\"".to_owned())
        } else if !map.contains_key("dependencies") {
            Err("Missing required field \"dependencies\"".to_owned())
        } else {
            if let Err(err) = validate_resolvers(map.get("resolvers").unwrap()).and(validate_dependencies(map.get("dependencies").unwrap())) {
                Err(err)
            } else {
                Ok(config_val)
            }
        }
    } else {
        Err(format!("Invalid JSON structure was expecting an json object but found: \n {}", config_val))
    }
}

fn validate_resolvers(resolver: &Value) -> Result<(), String> {
    if let Value::Array(arr) = resolver {
        if arr.iter().all(|val| val.is_string()) {
            let unknown_resolvers: Vec<&str> = arr.iter()
                .filter(|&val| !is_known_resolver(val.as_str().unwrap()))
                .map(|val| val.as_str().unwrap())
                .collect();
            if !unknown_resolvers.is_empty() {
                let unknown_resolvers_fmt = unknown_resolvers.join(",");
                Err(format!("Unknown resolvers: {} only \"Modrinth\" and \"Curseforge\" is supported", unknown_resolvers_fmt))
            } else {
                Ok(())
            }
        } else {
            Err("The \"resolvers\" field must be an array of strings".to_owned())
        }
    } else {
        Err("The \"resolvers\" field must be an array of strings".to_owned())
    }
}

fn validate_dependencies(depend: &Value) -> Result<(), String> {
    let mut invalid_fields: Vec<String> = Vec::new();
    if let Value::Object(map) = depend {
        for entry in map.iter() {
            if entry.1.is_null() {
                invalid_fields.push(format!("The version value isn't set for {}", entry.0));
            }
        }
    } else {
        invalid_fields.push("The \"dependencies\" field must be an object with \"<packageName>\":\"<version>\"".to_owned())
    }
    if invalid_fields.is_empty() {
        Ok(())
    } else {
        Err(invalid_fields.join("\n"))
    }
}

fn is_known_resolver(val: &str) -> bool {
    match val {
        "Modrinth" | "Curseforge" => true,
        _ => false
    }
}