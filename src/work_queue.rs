use std::collections::VecDeque;
use std::ops::{Deref, DerefMut};
use std::sync::RwLock;
use crate::dto::package::Package;

pub struct WorkQueue {
    packages: RwLock<VecDeque<Package>>
}

impl WorkQueue {
    pub fn new() ->  WorkQueue {
        WorkQueue {
            packages: RwLock::new(VecDeque::new())
        }
    }

    pub fn push(&mut self, package: Package) {
        let mut pack = self.packages.write().unwrap();
        pack.deref_mut().push_front(package)
    }

    pub fn push_all(&mut self, packages: & mut Vec<Package>) {
        let mut pack_rw = self.packages.write().unwrap();
        let curr_packages = pack_rw.deref_mut();
        while !packages.is_empty() {
            curr_packages.push_front(packages.pop().unwrap());
        }
    }

    pub fn pop(&mut self) -> Option<Package> {
        let mut pack_rw = self.packages.write().unwrap();
        pack_rw.deref_mut().pop_front()
    }

    pub fn is_empty(&self) -> bool {
        self.packages.read().unwrap().deref().is_empty()
    }
}